package com.proelan.wsdemo.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.proelan.wsdemo.WsdemoApplication;
import com.proelan.wsdemo.domain.User;
import com.proelan.wsdemo.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest(classes = WsdemoApplication.class)
public class UserResourceIT {
  @Autowired
  private UserRepository userRepository;

  @Autowired
  private MockMvc restUserMockMvc;

  // Mandatory fields
  private static final String DEFAULT_LOGIN = "LOGIN";
  private static final String DEFAULT_PASSWORD = "PASSWORD";
  private static final int DEFAULT_AGE = 18;
  private static final String DEFAULT_COUNTRY = "France";

  // Optional fields
  private static final String DEFAULT_FIRSTNAME = "FIRST_NAME";
  private static final String DEFAULT_EMAIL = "EMAIL";

  private static final ObjectMapper mapper = createObjectMapper();

  @AfterEach
  public void cleanUp() {
    userRepository.deleteAll();
  }

  // Test of User save
  @Test
  public void createValidUser() throws Exception {
    // Init
    int databaseSizeBeforeCreate = userRepository.findAll().size();

    // Create the User
    User user = new User();
    user.setLogin(DEFAULT_LOGIN);
    user.setPassword(DEFAULT_PASSWORD);
    user.setAge(DEFAULT_AGE);
    user.setResidenceCountry(DEFAULT_COUNTRY);

    user.setFirstName(DEFAULT_FIRSTNAME);
    user.setEmail(DEFAULT_EMAIL);

    // Run
    restUserMockMvc.perform(post("/api/register-user")
      .contentType(MediaType.APPLICATION_JSON)
      .content(convertObjectToJsonBytes(user)))
      .andExpect(status().isOk());

    // Verify: validate the User in the database
    assertPersistedUsers(users -> {
      assertThat(users).hasSize(databaseSizeBeforeCreate + 1);
      User testUser = users.get(users.size() - 1);
      assertThat(testUser.getLogin()).isEqualTo(DEFAULT_LOGIN);
      assertThat(testUser.getPassword()).isEqualTo(DEFAULT_PASSWORD);
      assertThat(testUser.getAge()).isEqualTo(DEFAULT_AGE);
      assertThat(testUser.getResidenceCountry()).isEqualTo(DEFAULT_COUNTRY);

      assertThat(testUser.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
      assertThat(testUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
    });
  }

  // Save invalid user should fail
  @Test
  public void createChildUser() throws Exception {
    // Init
    int databaseSizeBeforeCreate = userRepository.findAll().size();

    // Create the User
    User user = new User();
    user.setLogin(DEFAULT_LOGIN);
    user.setPassword(DEFAULT_PASSWORD);
    user.setAge(17);
    user.setResidenceCountry(DEFAULT_COUNTRY);

    user.setFirstName(DEFAULT_FIRSTNAME);
    user.setEmail(DEFAULT_EMAIL);

    // Run
    restUserMockMvc.perform(post("/api/register-user")
      .contentType(MediaType.APPLICATION_JSON)
      .content(convertObjectToJsonBytes(user)))
      .andExpect(status().isBadRequest());

    // Verify: validate the User in the database
    assertPersistedUsers(users -> assertThat(users).hasSize(databaseSizeBeforeCreate));
  }

  // Save invalid user should fail
  @Test
  public void createEnglishUser() throws Exception {
    // Init
    int databaseSizeBeforeCreate = userRepository.findAll().size();

    // Create the User
    User user = new User();
    user.setLogin(DEFAULT_LOGIN);
    user.setPassword(DEFAULT_PASSWORD);
    user.setAge(DEFAULT_AGE);
    user.setResidenceCountry("United Kingdom");

    user.setFirstName(DEFAULT_FIRSTNAME);
    user.setEmail(DEFAULT_EMAIL);

    // Run
    restUserMockMvc.perform(post("/api/register-user")
      .contentType(MediaType.APPLICATION_JSON)
      .content(convertObjectToJsonBytes(user)))
      .andExpect(status().isBadRequest());

    // Verify: validate the User in the database
    assertPersistedUsers(users -> assertThat(users).hasSize(databaseSizeBeforeCreate));
  }

  // Save invalid user should fail
  @Test
  public void createUserWithoutPassword() throws Exception {
    // Init
    int databaseSizeBeforeCreate = userRepository.findAll().size();

    // Create the User
    User user = new User();
    user.setLogin(DEFAULT_LOGIN);
    user.setPassword("");
    user.setAge(DEFAULT_AGE);
    user.setResidenceCountry(DEFAULT_COUNTRY);

    user.setFirstName(DEFAULT_FIRSTNAME);
    user.setEmail(DEFAULT_EMAIL);

    // Run
    restUserMockMvc.perform(post("/api/register-user")
      .contentType(MediaType.APPLICATION_JSON)
      .content(convertObjectToJsonBytes(user)))
      .andExpect(status().isBadRequest());

    // Verify: validate the User in the database
    assertPersistedUsers(users -> assertThat(users).hasSize(databaseSizeBeforeCreate));
  }

  // Test of User load
  @Test
  public void getUser() throws Exception {
    // Init
    // Create the User
    User user = new User();
    user.setLogin(DEFAULT_LOGIN);
    user.setPassword(DEFAULT_PASSWORD);
    user.setAge(DEFAULT_AGE);
    user.setResidenceCountry(DEFAULT_COUNTRY);

    user.setFirstName(DEFAULT_FIRSTNAME);
    user.setEmail(DEFAULT_EMAIL);

    // Initialize the database
    userRepository.save(user);

    // Run: get the user
    restUserMockMvc.perform(get("/api/get-users/{login}", user.getLogin()))

      // Verify
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.[*].login").value(hasItem(user.getLogin())))
      .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)))
      .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
      .andExpect(jsonPath("$.[*].residenceCountry").value(hasItem(DEFAULT_COUNTRY)))
      .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRSTNAME)))
      .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)));

  }

  private void assertPersistedUsers(Consumer<List<User>> userAssertion) {
    userAssertion.accept(userRepository.findAll());
  }

  /**
   * Convert an object to JSON byte array.
   *
   * @param object the object to convert.
   * @return the JSON byte array.
   * @throws IOException
   */
  public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
    return mapper.writeValueAsBytes(object);
  }

  /**
   * JSON mapper, for object comparisons.
    */
  private static ObjectMapper createObjectMapper() {
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS, false);
    mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
    mapper.registerModule(new JavaTimeModule());
    return mapper;
  }

}
