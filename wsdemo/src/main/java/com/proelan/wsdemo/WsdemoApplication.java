package com.proelan.wsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Run the app in an embedded Tomcat.
 */
@SpringBootApplication
public class WsdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsdemoApplication.class, args);
	}

}
