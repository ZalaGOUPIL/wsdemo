package com.proelan.wsdemo.web;

import com.proelan.wsdemo.domain.User;
import com.proelan.wsdemo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Our REST controller: loads and saves User object.
 */
@RestController
@RequestMapping("/api")
public class UserResource {

  private final Logger log = LoggerFactory.getLogger(UserResource.class);
  private UserService userService;

  public UserResource(UserService userService) {
    this.userService = userService;
  }

  /**
   * Register a new user.
   *
   * @param user to register
   * @return registered user
   */
  @PostMapping("/register-user")
  public ResponseEntity<User> registerUser(@Valid @RequestBody User user) {
    log.info("Request to save user : {}", user);
    if (user.getId() != null) {
      throw new IllegalArgumentException("A new user cannot already have an id!");
    }

    User registered = userService.saveUser(user);
    log.info("Saved user : {}", registered);
    return ResponseEntity.ok().body(registered);
  }

  /**
   * Get users' details
   * @param login to look for
   * @return all users found
   */
  @GetMapping("/get-users/{login}")
  public List<User> getUsers(@PathVariable String login) {
    return userService.findByLogin(login);
  }

  /**
   * Display human-readable validation messages for this controller.
   * @param e the exception to rewrite
   * @return key-value pairs of human-readable messages
   */
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleValidationErrors(
    MethodArgumentNotValidException e) {
    Map<String, String> validationErrors = new HashMap<>();

    e.getBindingResult().getAllErrors().forEach((error) -> {
      String fieldName = ((FieldError) error).getField();
      String errorMessage = error.getDefaultMessage();
      validationErrors.put(fieldName, errorMessage);
    });

    return validationErrors;
  }
}
