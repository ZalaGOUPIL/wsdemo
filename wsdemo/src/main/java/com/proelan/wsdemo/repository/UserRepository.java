package com.proelan.wsdemo.repository;

import com.proelan.wsdemo.domain.User;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Our User object repository: access to Neo4j DB
 */
@Repository
public interface UserRepository extends Neo4jRepository<User, String> {

  List<User> findAllByLogin(String login);

  void deleteAll();

}
