package com.proelan.wsdemo.service;

import com.proelan.wsdemo.domain.User;
import com.proelan.wsdemo.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Our User business service: load & save User from Neo4j DB.
 */
@Service
public class UserService {

  private UserRepository userRepository;

  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public User saveUser(User user) {
    return this.userRepository.save(user);
  }

  public List<User> findByLogin(String login) {
    return this.userRepository.findAllByLogin(login);
  }
}
